<!doctype html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>OBI</title>
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::style( 'css/bootstrap.min.css' ) }}
    {{ HTML::style( 'css/jumbotron.css' ) }}
    {{ HTML::script( 'js/bootstrap.min.js' ) }}
    @yield('cabecera')
      <style>
      body { padding-top: 65px; }
    </style>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-default navbar-inverse nav-justified navbar-fixed-top navbar-collapse" role="navigation">
        <div class="navbar-header">
          <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>-->
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
          </ul>
          
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Buscar">
            </div>
            <button type="submit" class="btn btn-default">Buscar</button>
          </form>

          <ul class="nav navbar-nav navbar-right">
            <!--<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </li>-->
          </ul>
        </div>
      </nav>  
      @yield('contenido')
      <div class="footer"><p> &copy; Scesi</p></div>
    </div>
  </body>
</html>
    