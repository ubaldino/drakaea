@extends('layouts.master')

@section('contenido')
	<a href='/pregunta' class="btn btn-primary"> lista de preguntas</a><br>
	 <div class="table-responsive">
	  <table class="table table-bordered table-striped table-hover">
	    <thead class="">
			<tr>
				
	    	<th>Código</th>
	    	<th>Nivel</th>
	    	<th>Tema</th>
	    	<th>Tipo</th>
	    	<th>Pregunta</th>
	    	<th>opción 1</th>
	    	<th>opción 2</th>
	    	<th>opción 3</th>
	    	<th>opción 4</th>
	    	<th>opción 5</th>
	    	<th>opción 6</th>
	    	<th class="danger">answer</th>
			</tr>
	    </thead>
	    <tbody>
	    	<tr>
	    	<td class="success">{{ $pregunta->pregunta}}</td>
			@foreach($pregunta->respuestas as $respuesta)
				<td class="warning">{{ $respuesta->respuesta }}  </td>
			@endforeach
			</tr>
	    </tbody>

	  </table>
	</div>

	<h3>Agregar respuesta</h3>
	<form method='post' action='' >
		<div class="form-group"></div>
			<input type='hidden' value='{{$pregunta->id}}' name='pregunta' class="form-control">
			<input type='text' name='respuesta' class="form-control" autofocus><br>
			<input type='submit' value='agregar respuesta' class="btn btn-primary">
		</div>
	</form>

@stop