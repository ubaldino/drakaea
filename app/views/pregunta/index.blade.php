<<<<<<< HEAD
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Metro UI CSS Framework">
    <meta name="author" content="jmejia, scesi">

    <link href="css/metro-bootstrap.css" rel="stylesheet">
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/jquery/jquery.widget.min.js"></script>
    <script src="js/metro/metro.min.js"></script>

    <title>preguntas para la OBI - Olimipiadas Bolivianas de Informatica </title>
</head>
<body class="metro">

<h2> Preguntas
<a href='/pregunta/agregar'><i class="icon-plus"></i> </a>
</h2>

<div class="listview">
@foreach($preguntas as $pregunta)
    <div class="list">
        <div class="list-content">
            <div class="data">
                <span class="list-title"> <a href="/pregunta/agregar-respuesta/{{$pregunta->id}}">{{$pregunta->pregunta}}</a></span>
                <span class="list-remark"> respuestas : {{ count($pregunta->respuestas) }} </span>
                <span class="list-remark"> tipo pregunta : {{ $pregunta->TipoPregunta->nombre }} </span>
            </div>
        </div>
    </div>
@endforeach
</div>
</body>
</html>
=======
@extends('layouts.master')

@section('contenido')
<h1> Preguntas </h1>
<a class="btn btn-primary" href='pregunta/agregar'> agregar pregunta  </a>
<ul class="list-group" >
	@foreach($preguntas as $pregunta)
		<li class="list-group-item">
			<span class="btn btn-default">	{{ $pregunta->pregunta }} </span>
			<div class="btn-group">
				<a class="btn btn-default" href='/pregunta/agregar-respuesta/{{$pregunta->id}}'> agregar respuesta<a>
				<a class="btn btn-default" href='/pregunta/elegir-respuesta/{{$pregunta->id}}'> elegir respuesta<a>
			</div>
		 </li>
	@endforeach
</ul>
@stop
>>>>>>> 1f01d14792c928ec8cee9bbdfc4109a7547d01d4
