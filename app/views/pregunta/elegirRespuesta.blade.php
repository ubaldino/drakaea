@extends('layouts.master')

@section('contenido')
	<p>{{$pregunta->pregunta }} <p>
	<form method='post' action=''>
		<div class="form-group" style="width: 40%;">
		<input type='hidden' name='pregunta' value='{{$pregunta->id }}' class="form-control">
		@if($pregunta->TipoPregunta->nombre == 'single')
			@foreach($pregunta->respuestas as $respuesta)
				 <input type="radio" name="es-corecto" value='{{ $respuesta->id}}'
				@if($respuesta->es_corecta)
					checked
				@endif 
				   > 
		                  {{ $respuesta->respuesta }}<br>
			@endforeach
		@endif
		@if($pregunta->TipoPregunta->nombre == 'multiple')
			@foreach($pregunta->respuestas as $respuesta)
				 <input type="checkbox" name='respuestas[]' value='{{$respuesta->id}}' 
				@if($respuesta->es_corecta)
					checked
				@endif 
				class="form-control" > 
		                  {{ $respuesta->respuesta }}<br>
			@endforeach
		@endif
		<input type='submit' value='guardar respuestas' class="btn btn-success">
	</div>
	</form>
@stop