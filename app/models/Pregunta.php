<?php

class Pregunta extends Eloquent {
	protected $table = 'pregunta';
	public $timestamps = false;
	
	public function respuestas() {
		return $this->hasMany('Respuesta');
	}
	public function tema() {
		return $this->belongsTo('Tema');
	}
	public function tipoPregunta() {
		return $this->belongsTo('TipoPregunta');
	}
}
