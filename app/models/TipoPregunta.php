<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class TipoPregunta extends Eloquent {
	protected $table = 'tipo_pregunta';
	public $timestamps = false;
	
	public function pregunta() {
		return $this->hasMany('Pregunta');
	}
}
