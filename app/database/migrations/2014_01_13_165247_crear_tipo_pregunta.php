<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTipoPregunta extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tipo_pregunta', function(Blueprint $table)
		{
			$table->create();
			$table->increments('id');
			$table->string('nombre');
		});
		DB::table('tipo_pregunta')->insert(
			array(
				array('id' => 1, 'nombre' => 'single'),
				array('id' => 2, 'nombre' => 'multiple')
		));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tipo_pregunta', function(Blueprint $table)
		{
			Schema::drop('tipo_pregunta');
		});
	}

}
