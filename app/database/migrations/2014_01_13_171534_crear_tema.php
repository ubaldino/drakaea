<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tema', function(Blueprint $table)
		{
			$table->create();
			$table->increments('id');
			$table->string('nombre');
		});
		DB::table('tema')->insert(array(
			array('id'=>1, 'nombre'=>'sistemas operativos'),
			array('id'=>2, 'nombre'=>'bash'),
			array('id'=>3, 'nombre'=>'libre office')
		));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tema', function(Blueprint $table)
		{
			Schema::drop('tema');
		});
	}

}
