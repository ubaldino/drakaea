<?php

class UsuarioController extends BaseController {
    public function getIndex() {
        return 'usuario';
    }

    public function getCrear() {
        return View::make('usuario.crear'); 
    }

    public function postCrear() {
         $email = Input::get('email');
         $password = Input::get('password');
         $usuario = new User;
         $usuario->email = $email;
         $usuario->password = Hash::make($password);
         $usuario->save();
         $data = array(
             'email' => $email,
             'password' => $password
         );
         
         if(Auth::attempt($data, true)) {
             return Redirect::to('/');
         }
         return Redirect::to('/');
    }

}

