<?php

class IndexController extends BaseController {
     public function getIndex() {
         if (Auth::check()) {
                $preguntas = Auth::user()->preguntas;
		return View::make('home.usuario')->with('preguntas', $preguntas);
         }
         return View::make('home.index');
     }
     public function getLogin() {
         return View::make('home.login');
     }
     
     public function postLogin() {
         $email = Input::get('email');
         $password = Input::get('password');
         $data = array(
             'email' => $email,
             'password' => $password   
         );
         if(Auth::attempt($data, true)) {
             return Redirect::to('/');
         }
         return Redirect::to('/');;
     }
     public function getLogout() {
         Auth::logout();
         return Redirect::to('/');
     }

     public function getRegistrar(){
         return View::make('home.registrar');
     }
     public function postRegistrar() {         
         $email = Input::get('email');
         $password = Input::get('password');
         $usuario = new User;
         $usuario->email = $email;
         $usuario->password = Hash::make($password);
         $usuario->save();         
         $data = array(
             'email' => $email,
             'password' => $password   
         );
         if(Auth::attempt($data, true)) {
             return Redirect::to('/');
         }
         return Redirect::to('/');;
     }
}
